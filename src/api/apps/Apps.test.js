import Apps from "./Apps";
import CATEGORIES from "./Categories";


test("fetchTotalAppCount should return the total number of apps.", async ()=> {
    await Apps.fetchAppList(0, 10);
    const totalAppCount = Apps.fetchTotalAppCount();
    expect(25).toEqual(totalAppCount);
});

test("fetchAppList should return the correct number of apps based on the offset and end.", async ()=> {
    const result = await Apps.fetchAppList(0, 10);
    expect(10).toEqual(result.length);
});

test("searchAndfilterAppsByCategoryAndName should return the correct values if we search by name.", async () => {
    const result = await Apps.searchAndfilterAppsByCategoryAndName([],"darwin",0 ,10);
    expect(1).toEqual(result.length);
    expect("Darwinbox").toEqual(result[0].name);
});

test("searchAndfilterAppsByCategoryAndName should return the correct values if we search by category.", async () => {
    const result = await Apps.searchAndfilterAppsByCategoryAndName([CATEGORIES.COMMUNICATION],"",0 ,10);
    expect(3).toEqual(result.length);
    expect(findAppByName(result, "Discord")).toBeDefined();
    expect(findAppByName(result, "Jira")).toBeDefined();
    expect(findAppByName(result, "Slack")).toBeDefined();
});

test("searchAndfilterAppsByCategoryAndName should return the correct values if we search by multiple categories.", async () => {
    const result = await Apps.searchAndfilterAppsByCategoryAndName([CATEGORIES.COMPLIANCE, CATEGORIES.COMMUNICATION],"",0 ,10);
    expect(6).toEqual(result.length);
    expect(findAppByName(result, "JuanTax")).toBeDefined();
    expect(findAppByName(result, "PwC")).toBeDefined();
    expect(findAppByName(result, "Taxumo")).toBeDefined();
    expect(findAppByName(result, "Discord")).toBeDefined();
    expect(findAppByName(result, "Slack")).toBeDefined();
});

test("searchAndfilterAppsByCategoryAndName should return the correct values if we search by name and category.", async () => {
    const result = await Apps.searchAndfilterAppsByCategoryAndName([CATEGORIES.COMMUNICATION],"Discord",0 ,10);
    expect(1).toEqual(result.length);
    expect(findAppByName(result, "Discord")).toBeDefined();
});

function findAppByName(result, appName) {
    return result.find((x) => { return x.name === appName});
}
