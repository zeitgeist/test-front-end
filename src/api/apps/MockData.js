import CATEGORIES from "./Categories";

const AppList = [
    {
        "name" : "Darwinbox",
        "categories": [CATEGORIES.HRIS, CATEGORIES.PAYROLL],
        "icon": "darwinbox.svg",
        "description": "Darwinbox provides end-to-end agile HRMS suite to deliver countless results within one platform for your employees."
    },
    {
        "name": "Discord",
        "categories": [CATEGORIES.COMMUNICATION],
        "icon": "discord.svg",
        "description": "Discord allows teams to collaborate through instant messaging and to oragnize topic-based channels to get daily updates."
    },
    {
        "name": "Filipay",
        "categories": [CATEGORIES.HRIS, CATEGORIES.PAYROLL],
        "icon": "filipay.svg",
        "description": "Filipay helps save time on doing manual time entries and provides robust solution for your workflow operations."
    },
    {
        "name": "JuanTax",
        "categories": [CATEGORIES.COMPLIANCE],
        "icon": "juantax.svg",
        "description": "JuanTax provides automated books of accounts, financial reports and other features that can help leverage your business."
    },
    {
        "name": "JustPayroll",
        "categories": [CATEGORIES.HRIS, CATEGORIES.PAYROLL],
        "icon": "justpayroll.svg",
        "description": "JustPayroll seamlessly stores records on HR, timekeeping and payroll data."
    },
    {
        "name": "NetSuite",
        "categories": [CATEGORIES.ACCOUNTING, CATEGORIES.ECOMMERCE],
        "icon": "netsuite.svg",
        "description": "NetSuite helps manage your business finances, operations, and customer relations through an integrated cloud business software suite."
    },
    {
        "name": "PayrollHero",
        "categories": [CATEGORIES.HRIS, CATEGORIES.PAYROLL],
        "icon": "payrollhero.svg",
        "description": "PayrollHero helps to streamline time, attendance and scheduling to manage your payroll easier."
    },
    {
        "name": "PhoenixHRIS",
        "categories": [CATEGORIES.HRIS, CATEGORIES.PAYROLL],
        "icon": "phoenixhris.png",
        "description": "PhoenixHRIS manages your payroll or HR process online that is best suited for your company."
    },
    {
        "name": "PwC",
        "categories": [CATEGORIES.COMPLIANCE],
        "icon": "pwc.svg",
        "description": "PwC provides services for tax compliance and accounting and payroll requirements that can offer knowledge and experience in local statutory regulations."
    },
    {
        "name": "QuickBooks Online",
        "categories": [CATEGORIES.BOOKKEEPING, CATEGORIES.PAYROLL],
        "icon": "quickbooksonline.svg",
        "description": "QuickBooks helps eliminate tedious manual data entry and so you can review everything in one place."
    },
    {
        "name": "Slack",
        "categories": [CATEGORIES.COMMUNICATION],
        "icon": "slack.png",
        "description": "Slack brings team communication and collaboration into one place so you can get more work done, whether you belong to a large enterprise or a small business."
    },
    {
        "name": "Jira",
        "categories": [CATEGORIES.COMMUNICATION],
        "icon": "jira.png",
        "description": "Jira is a proprietary issue tracking product developed by Atlassian that allows bug tracking and agile project management."
    },
    {
        "name": "Sprout",
        "categories": [CATEGORIES.HRIS,CATEGORIES.ACCOUNTING,CATEGORIES.PAYROLL],
        "icon": "sprout.png",
        "description": "Sprout Solutions is the #1 HR Analytics people platform in the Philippines that scales up your business with safe & secure end-to-end HR solutions, employee insights, and engagement."
    },
    {
        "name": "ZipHR",
        "categories": [CATEGORIES.HRIS],
        "icon": "ziphr.png",
        "description": "Revolutionary HR software and services platform that digitally transforms the way you recruit, hire, manage and pay your people."
    },
    {
        "name": "BambooHR",
        "categories": [CATEGORIES.HRIS],
        "icon": "bamboohr.jpeg",
        "description": "BambooHR's services include an applicant tracking system and an employee benefits tracker."    
    },
    {
        "name": "PeopleHum",
        "categories": [CATEGORIES.HRIS],
        "icon": "peoplehum.jpeg",
        "description": "PeopleHum's integrated attendance & leave management system lets you track absences without making it a hassle."    
    },
    {
        "name": "Zoho Books",
        "categories": [CATEGORIES.ACCOUNTING],
        "icon": "zohobooks.png",
        "description": "Zoho Books is online accounting software that manages your finances, automates business workflows, and helps you work collectively across departments."    
    },
    {
        "name": "Help Scout",
        "categories": [CATEGORIES.ECOMMERCE],
        "icon": "helpscout.png",
        "description": "The company provides an email-based customer support platform, knowledge base tool, and an embeddable search/contact widget for customer service professionals."    
    },
    {
        "name": "Potion",
        "categories": [CATEGORIES.ECOMMERCE],
        "icon": "potion.png",
        "description": "Potion adds the magic, generating a speedy site with custom domains, styles and great SEO."    
    },
    {
        "name": "uMake",
        "categories": [CATEGORIES.ECOMMERCE],
        "icon": "umake.png",
        "description": "uMake is the first design app to bring on-device rendering to iOS devices. Create beautiful, rendered images of your creations with a tap of a button."    
    },
    {
        "name": "Shopee",
        "categories": [CATEGORIES.ECOMMERCE],
        "icon": "shopee.png",
        "description": "Shopee is considered the largest e-commerce platform in Southeast Asia with 343 million monthly visitors."    
    },
    {
        "name": "Taxumo",
        "categories": [CATEGORIES.COMPLIANCE],
        "icon": "taxumo.png",
        "description": "Taxumo helps solo MSMEs, self-employed individuals, professionals, partnerships, and corporations easily compute, file, & pay for your taxes."    
    },
    {
        "name": "Lazada",
        "categories": [CATEGORIES.ECOMMERCE],
        "icon": "lazada.png",
        "description": "Lazada Group is one of the largest e-commerce operators in Southeast Asia, with over 10,000 third-party sellers as of November 2014, and 50 million annual active buyers as of September 2019."    
    },
    {
        "name": "Xero",
        "categories": [CATEGORIES.ACCOUNTING,CATEGORIES.PAYROLL],
        "icon": "xero.png",
        "description": "Xero's online accounting software connects small business owners with their numbers, their bank, and advisors anytime."    
    },
    {
        "name": "Rippling",
        "categories": [CATEGORIES.ACCOUNTING,CATEGORIES.PAYROLL,CATEGORIES.HRIS],
        "icon": "rippling.jpg",
        "description": "Rippling is an all-in-one HR platform that handles payroll, employee benefits, recruitment, training, and so much more."    
    }
];

export default AppList;