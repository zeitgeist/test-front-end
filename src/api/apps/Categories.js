const CATEGORIES = {
    ACCOUNTING: "Accounting",
    BOOKKEEPING: "Bookkeeping",
    COMMUNICATION: "Communication",
    COMPLIANCE: "Compliance",
    ECOMMERCE: "eCommerce",
    HRIS: "HRIS",
    PAYROLL: "Payroll"
};

export default CATEGORIES;
