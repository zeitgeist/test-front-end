import AppList from "./MockData";

let randomizedAppList = [];

const Apps = {
    fetchAppList: function(offset, end) {
        randomizedAppList = AppList.sort(() =>  Math.random() - 0.5);
        return paginateAndRespondAsPromise(randomizedAppList, offset, end);
    },
    fetchTotalAppCount: function() {
        return randomizedAppList.length;
    },
    searchAndfilterAppsByCategoryAndName: function(categoryList, searchQuery, offset, end) {
        let filteredList = [];
        
        if(searchQuery.length === 0 && categoryList.length === 0) {
            filteredList = AppList;
        }
        
        if(searchQuery.length > 0 && categoryList.length === 0) {
            filteredList = searchAppByName(searchQuery);
        }

        if(searchQuery.length === 0 && categoryList.length > 0) {
            filteredList = searchAppByCategories(categoryList, AppList);
        }

        if(searchQuery.length > 0 && categoryList.length > 0){
            filteredList = searchAppByCategoriesAndName(categoryList, searchQuery, AppList);
        }

        randomizedAppList = filteredList.flat();
        return paginateAndRespondAsPromise(randomizedAppList, offset, end);
    }
}

function searchAppByName(searchQuery) {
    return AppList.filter(({name}) => {
        return new RegExp(`${searchQuery}`, 'ig').test(name);
    });
}

function searchAppByCategories(categoryList, randomizedAppList) {
    let filteredList = [];

    categoryList.forEach((category) => {
        const foundApps = randomizedAppList.filter((app) => {         
            return app.categories.includes(category) && isUnique(filteredList, app);
        });

        filteredList.push(foundApps);
    });

    return filteredList;
}

function searchAppByCategoriesAndName(categoryList, searchQuery, randomizedAppList) {
    let filteredList = [];

    categoryList.forEach((category) => {
        const foundApps = randomizedAppList.filter((app) => {
            const likeName = new RegExp(`${searchQuery}`, 'ig').test(app.name);

            return app.categories.includes(category) 
            && isUnique(filteredList, app) 
            && likeName;
        });

        filteredList.push(foundApps);
    });

    return filteredList;
}
 
function isUnique(filteredList, app) {
    return filteredList.find(filteredApp => filteredApp.name === app.name) === undefined;
}

function paginateAndRespondAsPromise(randomizedAppList, offset, end) {
    const promise = new Promise((resolve, reject) => {
        resolve(randomizedAppList.slice(offset, end));
    });

    return promise;
}

export default Apps;