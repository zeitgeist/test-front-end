import { render, screen, waitFor } from "@testing-library/react";
import IntegrationsPage from "./IntegrationsPage";
import apps from "../../api/apps/Apps";
import mockData from "../../api/apps/MockData";

jest.mock("../../api/apps/Apps");

beforeEach(() => {
    apps.fetchAppList.mockResolvedValue(mockData);
    apps.fetchTotalAppCount.mockImplementation(() => mockData.length);
});

test("page should have the hero element.", async() => {
    render(<IntegrationsPage/>);

    await waitFor(() => expect(apps.fetchAppList).toHaveBeenCalled());
    await waitFor(() => expect(apps.fetchTotalAppCount).toHaveBeenCalled());
    
    const hero = screen.getByTestId("hero");
    expect(hero).toBeInTheDocument();
});

test("page should have the showing element.", async() => {
    render(<IntegrationsPage/>); 

    await waitFor(() => expect(apps.fetchAppList).toHaveBeenCalled());
    await waitFor(() => expect(apps.fetchTotalAppCount).toHaveBeenCalled());
    
    const showing = screen.getByTestId("showing");
    expect(showing).toBeInTheDocument();
});

test("page should have the search-field element.", async() => {
    render(<IntegrationsPage/>); 

    await waitFor(() => expect(apps.fetchAppList).toHaveBeenCalled());
    await waitFor(() => expect(apps.fetchTotalAppCount).toHaveBeenCalled());
    
    const searchField = screen.getByTestId("search-field");
    expect(searchField).toBeInTheDocument();
});

test("page should have the filter-chips element.", async() => {
    render(<IntegrationsPage/>); 

    await waitFor(() => expect(apps.fetchAppList).toHaveBeenCalled());
    await waitFor(() => expect(apps.fetchTotalAppCount).toHaveBeenCalled());
    
    const filterChips = screen.getByTestId("filter-chips");
    expect(filterChips).toBeInTheDocument();
});

test("page should have the integration-apps element.", async() => {
    render(<IntegrationsPage/>); 

    await waitFor(() => expect(apps.fetchAppList).toHaveBeenCalled());
    await waitFor(() => expect(apps.fetchTotalAppCount).toHaveBeenCalled());
    
    const integrationApps = screen.getByTestId("integration-apps");
    expect(integrationApps).toBeInTheDocument();
});

