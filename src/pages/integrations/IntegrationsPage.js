import { Grid, Pagination } from "@mui/material";
import { useEffect, useState } from "react";
import Apps from "../../api/apps/Apps";
import AppIntegration from "../../components/appIntegration/AppIntegration";
import EmptyPageFiller from "../../components/emptyPageFiller/EmptyPageFiller";
import FilterChip from "../../components/filterChip/FilterChip";
import Hero from "../../components/hero/Hero";
import SearchField from "../../components/searchField/SearchField";
import Showing from "../../components/showing/Showing";
import CATEGORIES from "../../api/apps/Categories";
import "./IntegrationsPage.css";

function IntegrationsPage() {

    const INITIAL_OFFSET = 0;
    const ITEMS_PER_PAGE = 10;
    const INITIAL_PAGE = 1;

    const [appList, setAppList] = useState([]);
    const [currentPage, setCurrentPage] = useState(INITIAL_PAGE);
    const [pageCount, setPageCount] = useState(0);
    const [totalAppCount, setTotalAppCount] = useState(0);
    const [currentOffset, setCurrentOffset] = useState(0);

    const [searchQuery, setSearchQuery] = useState("");

    const [isFilterOpen, setIsFilterOpen] = useState(false);
    const [checkboxStates, setCheckBoxStates] = useState(getDefaultCheckboxStates());
    const [filterChips, setFilterChips] = useState(null);
    const [chipToRemove, setChipToRemove] = useState(null);

    useEffect(() => {
        const totalAppCount = Apps.fetchTotalAppCount()
        const pageCount = calculatePageCount(totalAppCount);

        Apps
        .fetchAppList(INITIAL_OFFSET,ITEMS_PER_PAGE)
        .then((result) => {
            setCurrentPage(INITIAL_PAGE);
            setTotalAppCount(totalAppCount);
            setPageCount(pageCount);
            setAppList(result);
        });

    },[]);

    useEffect(() => {
        if(chipToRemove === null) {
            return;
        }

        setFilterChips(renderFilterChips());
        searchAndFilter(currentPage);
    }, [chipToRemove]);

    function handlePageChange(event, page) {
        searchAndFilter(page);
    }

    function onKeyEnter(event) {
        event.preventDefault();

        searchAndFilter(INITIAL_PAGE);
    }

    function searchAndFilter(page) {
        const newOffset = calculateOffset(page);
        const newEnd = calculateNewEnd(newOffset);
        const checkedBoxes = getCheckedBoxes();

        Apps
        .searchAndfilterAppsByCategoryAndName(checkedBoxes, searchQuery, newOffset, newEnd)
        .then((result) => {
            const totalAppCount = Apps.fetchTotalAppCount();
            const pageCount = calculatePageCount(totalAppCount);

            setTotalAppCount(totalAppCount);
            setCurrentOffset(newOffset);
            setCurrentPage(page);
            setPageCount(pageCount);
            setAppList(result);

        });
    }

    function getDefaultCheckboxStates() {
        return {
            [CATEGORIES.ACCOUNTING] : false,
            [CATEGORIES.BOOKKEEPING]: false,
            [CATEGORIES.COMMUNICATION]: false,
            [CATEGORIES.COMPLIANCE]: false,
            [CATEGORIES.ECOMMERCE]: false,
            [CATEGORIES.HRIS]: false,
            [CATEGORIES.PAYROLL]: false
        };
    }

    function calculateOffset(page) {
        if(totalAppCount !== 0) {
            return ((page - 1) * ITEMS_PER_PAGE) % totalAppCount;
        }
        return 0;
    }

    function calculateNewEnd(offset) {
        return offset + (ITEMS_PER_PAGE);
    }

    function calculatePageCount(totalAppCount) {
        return Math.ceil(totalAppCount / ITEMS_PER_PAGE);
    }

    function onChangeQuery(event) {
        setSearchQuery(event.target.value);
    }

    function onCheckBoxChange(event) {
        setCheckBoxStates({
            ...checkboxStates,
            [event.target.name]: event.target.checked
        });
    }

    function onClearClick() {
        setCheckBoxStates(getDefaultCheckboxStates());
        setFilterChips(null);
    }

    function onFilterClick(event) {
        event.preventDefault();
        setIsFilterOpen(!isFilterOpen);
    }

    function onApplyClick(event) {
        searchAndFilter(INITIAL_PAGE);
        setFilterChips(renderFilterChips());
    }

    function getCheckedBoxes() {
        const keys = Object.keys(checkboxStates);
        let checkedBoxes = [];

        keys.forEach((key) => {
            if(checkboxStates[key] === true) {
                checkedBoxes.push(key);
            }
        });

        return checkedBoxes;
    }

    function onClickOutsideFilter() {
        setIsFilterOpen(false);
    }

    function renderAppList() {
        return appList && appList.map((app) =>
            <Grid item xs={12} sm={6} md={4} key={app.name}>
                <AppIntegration data-testid="app-integration" app={app} key={app.name}/>
            </Grid>
        );
    }

    function onChipCloseClick(closedChip) {
        let states = {
            ...checkboxStates,
            [closedChip]: false
        };
        
        setCheckBoxStates(states);
        setChipToRemove(closedChip);
    }

    function renderFilterChips() {
        const checkedBoxes = getCheckedBoxes();

        return checkedBoxes.map((checkedBox) => 
            <FilterChip key={checkedBox} textBody={checkedBox} onCloseClick={onChipCloseClick} />
        );
    }

    return <div >
        <Hero/>

        <div className="integrations-page-limiter">
            <Showing
                offset={currentOffset}
                itemsPerPage={appList.length}
                totalCount={totalAppCount}
            />

            <SearchField 
                className="search-field" 
                textFieldValue={searchQuery} 
                onTextFieldChange={onChangeQuery}
                checkBoxStates={checkboxStates}
                onCheckBoxChange={onCheckBoxChange}
                isFilterOpen={isFilterOpen}
                onFilterClick={onFilterClick}
                onClickOutsideFilter={onClickOutsideFilter}
                onClearClick={onClearClick}
                onApplyClick={onApplyClick}
                onKeyEnter={onKeyEnter}  
            />

            <div className="filter-chips-container" data-testid="filter-chips">
                {filterChips}
            </div>
            
            <Grid container spacing={3} className="integrations-container" data-testid="integration-apps" gridAutoRows={100}>
                {renderAppList()}
            </Grid>
            

            { appList.length > 0 ?
                <Pagination 
                    data-testid="pagination"
                    className="pagination" 
                    page={currentPage}
                    onChange={handlePageChange}
                    count={pageCount} 
                    color="primary"
                />  : <EmptyPageFiller/>  
            }

        </div>
    </div>
}

export default IntegrationsPage;