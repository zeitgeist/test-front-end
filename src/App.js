import { createTheme, ThemeProvider } from "@mui/material/styles";
import "./App.css";
import IntegrationsPage from "./pages/integrations/IntegrationsPage";

function App() {

  const theme = createTheme({
    palette: {
      primary: {
        main: "#4B68FE",
      }
    }
  });

  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <IntegrationsPage/>
      </ThemeProvider>
    </div>
  );
}

export default App;