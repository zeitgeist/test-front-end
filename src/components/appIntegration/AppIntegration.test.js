import { render, screen } from "@testing-library/react";
import AppIntegration from "./AppIntegration";

const app = {
    "name": "Darwinbox",
    "description": "some description",
    "icon": "darwinbox.svg",
    "categories": ["HRIS", "Communication"]
};

test("should render the app name.", () => {
    render(<AppIntegration app={app} />);
    const linkElement = screen.getByText("Darwinbox");
    expect(linkElement).toBeInTheDocument();
});

test("should render the app description.", () => {
    render(<AppIntegration app={app} />);
    const linkElement = screen.getByText("some description");
    expect(linkElement).toBeInTheDocument();
});

test("should render the app categories correctly.", () => {
    render(<AppIntegration app={app} />);
    const linkElement = screen.getByText("HRIS, Communication");
    expect(linkElement).toBeInTheDocument();
});

test("should render the app's image icon.", () => {
    render(<AppIntegration app={app} />);
    const linkElement = screen.getByAltText("app icon preview");
    expect(linkElement).toBeInTheDocument();
});
