import { Card, CardActions, CardContent } from "@mui/material";
import { useState } from "react";
import NotifyLink from "../notifyLink/NotifyLink";
import NotifyMeModal from "../notifyMeModal/NotifyMeModal";
import './AppIntegration.css';

function AppIntegration({app}) {

    const [isModalOpen, setIsModalOpen] = useState(false);

    function notifyMe() {
        setIsModalOpen(true);
    }

    function closeNotifyMeModal() {
        setIsModalOpen(false);
    }

    return <Card className="card">
            <CardContent className="card-content">
                <div className="group-content">
                    <div className="image-container">
                        <img src={require('../../assets/apps/' + app.icon)} alt="app icon preview"/>
                    </div>
                    <div className="title-container">
                        <div className="name-container">
                            {app.name}
                        </div>
                        <div className="category-container">
                            {app.categories && app.categories.join(", ")}
                        </div>
                    </div>
                </div>
                <div className="description">
                    {app.description}
                </div>
            </CardContent>
            <CardActions>
                <NotifyLink onClick={notifyMe} text="Notify me when it's ready >"/>
                <NotifyMeModal close={closeNotifyMeModal} open={isModalOpen}/>
            </CardActions>
    </Card>
}

export default AppIntegration;