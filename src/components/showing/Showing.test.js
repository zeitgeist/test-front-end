import { render, screen } from "@testing-library/react";
import Showing from "./Showing";

test("should render the correct start value.", () => {
  render(<Showing offset={0} itemsPerPage={10} totalCount={25}/>);
  const linkElement = screen.getByText("Showing 1 - 10 of 25 apps");
  expect(linkElement).toBeInTheDocument();
});