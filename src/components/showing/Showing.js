import { useEffect, useState } from "react";
import "./Showing.css";

function Showing({offset, itemsPerPage, totalCount}) {

    const [start, setStart] = useState(0);
    const [end, setEnd] = useState(0);

    useEffect(() => {
        setStart(calculateStart());
        setEnd(calculateEnd());
    });

    function calculateStart() {
        return offset + 1;
    }

    function calculateEnd() {
        return (start + itemsPerPage) - 1;
    }

    return <div className="showing-container" data-testid="showing">
        Showing {start} - {end} of {totalCount} apps 
    </div>;
}

export default Showing;