import { render, screen } from "@testing-library/react";
import EmptyPageFiller from "./EmptyPageFiller";

test("should render the empty page title.", () => {
  render(<EmptyPageFiller />);
  const linkElement = screen.getByText("We can't seem to find what you're looking for.");
  expect(linkElement).toBeInTheDocument();
});

test("should render the empty page subtitle.", () => {
  render(<EmptyPageFiller />);
  const linkElement = screen.getByText("Try changing the filters or search terms.");
  expect(linkElement).toBeInTheDocument();
});