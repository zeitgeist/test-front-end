import "./EmptyPageFiller.css";

function EmptyPageFiller() {
    return <div className="empty-page-container" data-testid="empty-page">
        <div className="image-container">
            <img src={require('../../assets/emptyPage/empty_page_illustration.png')} alt="A girl looking at a paper with her glasses."/>
        </div>
        <div className="empty-page-title">
            We can't seem to find what you're looking for.
        </div>
        <div className="empty-page-subtitle">
            Try changing the filters or search terms.
        </div>
    </div>
}

export default EmptyPageFiller;