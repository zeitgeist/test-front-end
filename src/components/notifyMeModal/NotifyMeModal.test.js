import { render, screen } from "@testing-library/react";
import NotifyMeModal from "./NotifyMeModal";

const mockCallBack = jest.fn();

test("should render the title correctly.", () => {
  render(<NotifyMeModal open={true} close={mockCallBack}/>);
  const linkElement = screen.getByText("Thank you for your interest");
  expect(linkElement).toBeInTheDocument();
});

test("should render the subtitle correctly.", () => {
    render(<NotifyMeModal open={true} close={mockCallBack}/>);
    const linkElement = screen.getByText("We're still working on integrating this app and it's not yet ready for now. But you can send us your email so we can let you know when it's ready.");
    expect(linkElement).toBeInTheDocument();
});

test("should render the email text field correctly.", () => {
    render(<NotifyMeModal open={true} close={mockCallBack}/>);
    const linkElement = screen.getByLabelText("Email");
    expect(linkElement).toBeInTheDocument();
});

test("should render the nevermind button correctly.", () => {
    render(<NotifyMeModal open={true} close={mockCallBack}/>);
    const linkElement = screen.getByRole("button", {name: "Never mind"});
    expect(linkElement).toBeInTheDocument();
});

test("should render the notify me button correctly.", () => {
    render(<NotifyMeModal open={true} close={mockCallBack}/>);
    const linkElement = screen.getByRole("button", {name: "Notify me"});
    expect(linkElement).toBeInTheDocument();
});

