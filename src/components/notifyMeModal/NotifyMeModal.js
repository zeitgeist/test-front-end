import {Button, Modal, TextField } from "@mui/material";
import "./NotifyMeModal.css";
import CloseIcon from "@mui/icons-material/Close";
import SendIcon from "@mui/icons-material/Send";

function NotifyMeModal({open, close}) {

    function notifyMe() {
        // TODO: Implement me.
    }

    return <Modal
            open={open}
            onClose={close}
            aria-labelledby="Notify Me Modal"
            aria-describedby="A modal for notifying the user."
        >
        <div className="notify-me-container">
            <div className="close-button-container">
                <button onClick={close}>
                    <CloseIcon/>
                </button>
            </div>

            <h4 className="title">
                Thank you for your interest
            </h4>
            <p className="subtitle">
                We're still working on integrating this app and it's not yet ready for now. But you can send us your email so we can let you know when it's ready.
            </p>

            <TextField className="email-field" label="Email" placeholder="email@email.com"/>

            <div className="actions-container">
                <Button variant="outlined" className="nevermind-button" onClick={close}>Never mind</Button>
                <Button variant="contained" className="notify-me-button" onClick={notifyMe} startIcon={<SendIcon />} disableElevation>Notify me</Button>
            </div>
        </div>
    </Modal>
}

export default NotifyMeModal;