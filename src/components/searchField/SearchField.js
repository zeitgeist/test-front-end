import SearchIcon from "@mui/icons-material/Search";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import "./SearchField.css";
import { Button, Checkbox, FormControlLabel, FormGroup } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { useEffect, useRef } from "react";

function SearchField({
    textFieldValue, 
    onTextFieldChange, 
    checkBoxStates, 
    onCheckBoxChange, 
    isFilterOpen,
    onFilterClick,
    onClickOutsideFilter,
    onClearClick,
    onApplyClick,
    onKeyEnter
}) {

    const filterRef = useRef(null);

    useEffect(() => {
        const handleClickOutside = (event) => {
            if(filterRef.current && !filterRef.current.contains(event.target)) {
                onClickOutsideFilter && onClickOutsideFilter();
            }
        };

        document.addEventListener('click', handleClickOutside, true);
        return () => {
            document.removeEventListener('click', handleClickOutside, true);
        }
    },[onClickOutsideFilter]);

    const checkboxes = Object.keys(checkBoxStates).map((key) => {
        return <FormControlLabel 
            label={key} 
            labelPlacement="end" 
            value={key}
            key={key}
            disabled={false}
            control={<Checkbox checked={checkBoxStates[key]} onChange={onCheckBoxChange} name={key} key={key} label={key}/>}
        />;
    });

    return <div data-testid="search-field">
        <div className="search-field-container">
            <form onSubmit={onKeyEnter}> 
                <input className="search-field" type="text" value={textFieldValue} onChange={onTextFieldChange} placeholder="Search apps" />
            </form>
            <button className="filter-button" onClick={onFilterClick}>Filter<ArrowDropDownIcon className="arrow-dropdown-icon"/></button>
            <SearchIcon className="search-icon"/>
        </div>
        <div ref={filterRef} className={isFilterOpen ? "filter-container active" : "filter-container"}>
            <div className="filter-header">
                <div className="filter-header-title">
                    Filters
                </div>
                <div className="filter-close-button-container">
                    <CloseIcon onClick={onFilterClick}/>
                </div>
            </div>
            <div className="filter-label-container">
                <div className="filter-header-label">
                    Category
                </div>
                <div className="filter-clear-button">
                    <button onClick={onClearClick}>Clear</button>
                </div>
            </div>

            <FormGroup className="filter-form-group">
                {checkboxes}
            </FormGroup>
            
            <div className="filter-actions">
                <div>
                    <Button className="clear-all-button" onClick={onClearClick}>Clear all</Button>
                </div>
                <div>
                    <Button className="apply-button" onClick={onApplyClick} variant="contained" disableElevation>Apply</Button>
                </div>
            </div>
        </div>
    </div>

}

export default SearchField;