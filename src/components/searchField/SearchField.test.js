import { render, screen } from "@testing-library/react";
import SearchField from "./SearchField";

const mockCallBack = jest.fn();
const checkBoxStates = {
    Communication: false,
    HRIS: false
}

test("should render the search apps input box.", () => {
    const {container} = render(<SearchField 
        textFieldValue="darwinbox" 
        onTextFieldChange={mockCallBack}
        checkBoxStates={checkBoxStates}
        onCheckBoxChange={mockCallBack}
        isFilterOpen={mockCallBack}
        onFilterClick={mockCallBack}
        onClickOutsideFilter={mockCallBack}
        onClearClick={mockCallBack}
        onApplyClick={mockCallBack}
        onKeyEnter={mockCallBack}  
    />);
    expect(container.getElementsByClassName("search-field").length).toBe(1);
});

test("should render the filter button.", () => {
    const {container} = render(<SearchField 
        textFieldValue="darwinbox" 
        onTextFieldChange={mockCallBack}
        checkBoxStates={checkBoxStates}
        onCheckBoxChange={mockCallBack}
        isFilterOpen={mockCallBack}
        onFilterClick={mockCallBack}
        onClickOutsideFilter={mockCallBack}
        onClearClick={mockCallBack}
        onApplyClick={mockCallBack}
        onKeyEnter={mockCallBack}  
    />);
    expect(container.getElementsByClassName("filter-button").length).toBe(1);
});

test("should render the checkboxes filter.", () => {
    render(<SearchField 
        textFieldValue="darwinbox" 
        onTextFieldChange={mockCallBack}
        checkBoxStates={checkBoxStates}
        onCheckBoxChange={mockCallBack}
        isFilterOpen={mockCallBack}
        onFilterClick={mockCallBack}
        onClickOutsideFilter={mockCallBack}
        onClearClick={mockCallBack}
        onApplyClick={mockCallBack}
        onKeyEnter={mockCallBack}  
    />);

    const communicationCheckBox = screen.getByText("Communication");
    expect(communicationCheckBox).toBeInTheDocument();

    const hrisCheckBox = screen.getByText("HRIS");
    expect(hrisCheckBox).toBeInTheDocument();
});