import "./NotifyLink.css";

function NotifyLink({onClick, text}) {
    return <button className="notify-link" onClick={onClick}>{text}</button>
}

export default NotifyLink;