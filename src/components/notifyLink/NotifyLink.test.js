import { render, screen } from "@testing-library/react";
import NotifyLink from "./NotifyLink";

test("should render the notify link text correctly.", () => {
  render(<NotifyLink text="hello"/>);
  const linkElement = screen.getByText("hello");
  expect(linkElement).toBeInTheDocument();
});
