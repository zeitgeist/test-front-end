import { render, screen } from "@testing-library/react";
import FilterChip from "./FilterChip";

const mockCallBack = jest.fn();

test("should render the filter chip text body.", () => {
    render(<FilterChip textBody="Communication" onCloseClick={mockCallBack}/>);
    const linkElement = screen.getByText("Category: Communication");
    expect(linkElement).toBeInTheDocument();
});

test("should render the x button.", () => {
    const {container} = render(<FilterChip textBody="Communication" onCloseClick={mockCallBack}/>);
    expect(container.getElementsByClassName("filter-chip-close-button").length).toBe(1);
});