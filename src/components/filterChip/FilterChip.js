import CloseIcon from '@mui/icons-material/Close';
import "./FilterChip.css";

function FilterChip({textBody, onCloseClick}) {
    return <div className="filter-chip">
        Category: {textBody} <CloseIcon onClick={() => onCloseClick(textBody)} className="filter-chip-close-button"/>
    </div>
}

export default FilterChip;