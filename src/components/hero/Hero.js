import './Hero.css';

function Hero() {
    return <div className="hero-container" data-testid="hero">
        <div className="hero-limiter">
            <div>
                <h1 className="hero-title">
                    Integrations
                </h1>
                <p className="hero-description">
                    We plan to connect NextPay with your favorite software. If there's an app you'd love to integrate with us, let us know and we'll notify you as soon as it's available!
                </p>
            </div>
            <div className="hero-image-container">
                <img className="hero-image" src={require('../../assets/integrations/integrations-illustration.svg').default} alt="images of apps that can be used for integration."/>
            </div>
        </div>
    </div>
}

export default Hero;