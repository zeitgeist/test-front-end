import { render, screen } from "@testing-library/react";
import Hero from "./Hero";

test("should render the hero title.", () => {
  render(<Hero />);
  const linkElement = screen.getByText("Integrations");
  expect(linkElement).toBeInTheDocument();
});

test("should render the hero subtitle.", () => {
  render(<Hero />);
  const linkElement = screen.getByText("We plan to connect NextPay with your favorite software. If there's an app you'd love to integrate with us, let us know and we'll notify you as soon as it's available!");
  expect(linkElement).toBeInTheDocument();
});
